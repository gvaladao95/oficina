#!/usr/bin/python
# -*- coding: utf-8 -*-

import telebot, sys
import threading
import time
from threading import Thread
import socket
import os
import glob
import time
from datetime import datetime
import RPi.GPIO as GPIO
from cryptography.fernet import Fernet


ajuste=0.75

Temperatura1 = 0
Temperatura2 = 0
AlertaAlto=0
TempAlerta=17.0


from senhas import Chaves
bot = telebot.TeleBot(Chaves[0], parse_mode=None)

HOST = '192.168.2.132'              # Endereco IP do Servidor
PORT = 5001            # Porta que o Servidor esta

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

tcp.bind((HOST, PORT))

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')


import smtplib
from email.mime.text import MIMEText

def EnviaEmail():

        try:
                smtp_ssl_host = 'smtp.gmail.com'
                smtp_ssl_port = 465

                from senhas import Chaves
                username = Chaves[2] # Login da conta a ser usada
                passwd = Chaves[1] # Senha

                from_addr = Chaves[2] # Endereço que envia / mesmo que em username
                to_addr = Chaves[2] # Endereço de destino

                message = MIMEText("O sistema detectou um aumento na temperatura do ambiente.\nAlerta programado para temperaturas acima de 26ºC.")
                message['subject'] = 'Alerta Temperatura'
                message['from'] = from_addr
                message['to'] = to_addr

                server = smtplib.SMTP_SSL(smtp_ssl_host, smtp_ssl_port)

                server.login(username,passwd)
                server.sendmail(from_addr,to_addr,message.as_string())
                server.quit()
        except:
                print("Problema ao enviar alerta via email. Confira o email e a senha. \nVerifique se o servidor de email não está bloqueando o envio.")
                pass


def read_temp_raw(devicenumber):
        base_dir = '/sys/bus/w1/devices/'
        device_folder = glob.glob(base_dir + '28*')[devicenumber]
        device_file = device_folder + '/w1_slave'
        f = open(device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

def FuncRetornaTemp(numero):
        global bot
        global ArquivoLog
        global Temperatura1
        global Temperatura2
        global AlertaAlto

        lines = read_temp_raw(numero)
        while lines[0].strip()[-3:] != 'YES':
                time.sleep(0.2)
                lines = read_temp_raw(numero)
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
                temp_string = lines[1][equals_pos+2:]
                temp_c = float(temp_string) / 1000.0

                ArquivoLog = open('arqLog.txt','a')
                ArquivoLog.write("S1: " + str(Temperatura1) + " C. " + "S2: " + str(Temperatura2) + " C - " + str(datetime.now()) + "\n")
                ArquivoLog.close()




        #print(str(AlertaAlto))
        if( ( float(Temperatura1) >= float(TempAlerta) or float(Temperatura2) >= float(TempAlerta)) and AlertaAlto==0):
                print("Entrou " + str(Temperatura1))
                AlertaAlto=1000
                bot.send_message(1697735067, "Alerta de temperatura alta! " + str(Temperatura1) + " C" + " e " + str(Temperatura2) + " C. - " + str(datetime.now()))
                bot.send_message(1697735067, "Alerta de temperatura alta! " + str(Temperatura1) + " C" + " e " + str(Temperatura2) + " C. - " + str(datetime.now()))

                bot.send_message(1806520750, "Alerta de temperatura alta! " + str(Temperatura1) + " C" + " e " + str(Temperatura2) + " C. - " + str(datetime.now()))

                bot.send_message(1806520750, "Alerta de temperatura alta! " + str(Temperatura1) + " C" + " e " + str(Temperatura2) + " C. - " + str(datetime.now()))

#                bot.send_message(1806520750, "Alerta de temperatura alta! " + str(Temperatura1) + " C" + " e " + str(Temperatura2) + " C. - " + str(datetime.now()))
#                bot.send_message(1806520750, "Alerta de temperatura alta! " + str(Temperatura1) + " C" + " e " + str(Temperatura2) + " C. - " + str(datetime.now()))


                EnviaEmail()

                GPIO.setmode(GPIO.BCM)
                GPIO.setwarnings(False)
                GPIO.setup(18,GPIO.OUT)

                for i in range(10):
                        GPIO.output(18,GPIO.HIGH)
                        time.sleep(0.35)
                        GPIO.output(18,GPIO.LOW)
                        time.sleep(0.2)

        elif(AlertaAlto>0):
                AlertaAlto-=1

        return temp_c

def encrypt(dados):
        f = Fernet(Chaves[3])
        encrypted_message = f.encrypt(dados)
        return encrypted_message

def ServerSocket():
        global Temperatura1
        global Temperatura2


        time.sleep(1)

        tcp.listen(1)
        conexao, host = tcp.accept()
        #print('Conectado por', host)
        try:
                dados = str("Sensor 1: ") + str(Temperatura1) + str(" Sensor 2: ") + str(Temperatura2)
                print('Enviou: ', dados)

                enc = encrypt(dados.encode())
                conexao.send(enc)

                #conexao.send(dados.encode())
        except KeyboardInterrupt:
                print("\nInterrompendo o programa (ctrl+c).")
                time.sleep(2)
                tcp.close()
                exit()
        except socket.error as e:
                print('Erro geral no Socket.')
                time.sleep(2)
                tcp.close()
                pass
        except:
                time.sleep(2)
                tcp.close()
                pass



def FuncThreadTelegram():
        print("THREADTELEGRAM")
        global bot
        global Temperatura1
        global Temperatura2

        @bot.message_handler(func=lambda m: True)
        def echo_all(message):
                if(message.text=="temperatura" or message.text=="Temperatura" or message.text=="temp" or message.text=="Temp"):
                        #bot.send_message(message.chat.id, message.text)
                        print(str(message.chat.id) + " Escreveu: " + message.text)
                        #read_temp(0) + "e" + str(read_temp(1))
                        bot.reply_to(message, str(Temperatura1) + " ºC" +  " e " + str(Temperatura2) + " ºC")
                elif(message.text=="Histórico" or message.text=="histórico" or message.text=="Historico" or message.text=="historico" or message.text=="hist" or message.text=="Hist"):
                        print(str(message.chat.id) + " Escreveu: " + message.text)
                        bot.reply_to(message, "Temperatura do dia detalhada: ")

                        ArquivoLog = open('arqLog.txt','rb')

                        bot.send_document(message.chat.id, ArquivoLog)

                        ArquivoLog.close()

                else:
                        print(str(message.chat.id) + " Escreveu: " + message.text)
                        bot.reply_to(message, "Digite um dos seguintes comandos: temperatura ou histórico")

        bot.polling()



def FuncThreadTemp():
        print("THREADTEMP")
        global Temperatura1
        global Temperatura2

        while True:
                Temperatura1 = FuncRetornaTemp(0) + ajuste
                #print(Temperatura1)
                Temperatura2 = FuncRetornaTemp(1)
                #print(Temperatura2)
                time.sleep(0.5)


def FuncThreadSocketHost():
        print("THREADSOCKETHOST")
        try:
                while True:
                        ServerSocket()
                        time.sleep(1)

        except KeyboardInterrupt:
                print("\nInterrompendo o programa.")
                time.sleep(2)
                tcp.close()
                exit()
        except :
                print("\nErro geral na Thread SocketHost.")
                time.sleep(2)
                tcp.close()
                pass








ThreadTemp = threading.Thread(target=FuncThreadTemp)
ThreadTemp.start()

ThreadTelegram = threading.Thread(target=FuncThreadTelegram)
ThreadTelegram.start()

ThreadSocketHost = threading.Thread(target=FuncThreadSocketHost)
ThreadSocketHost.start()

while ThreadTemp.isAlive() and ThreadTelegram.isAlive() and ThreadSocketHost.isAlive():
#       print("Aguardandos")
        time.sleep(0.3)

print("\nThreads terminaram!!!")

tcp.close()

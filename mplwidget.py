from PyQt5.QtWidgets import QSizePolicy
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure


lista=[]
lista.append('')


class MplWidget(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=1, height=1, dpi=75):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(1,1,1)


        super(MplWidget, self).__init__(fig)
        self.setParent(parent)
        FigureCanvasQTAgg.setSizePolicy(self,
                                   QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvasQTAgg.updateGeometry(self)

    def update_figure(self, hora, temp1, temp2):
        global lista
        self.axes.cla()
        # print(temp1)
        # print(temp2)
        
        
        for i in range(len(temp1)):
            temp1[i]=float(temp1[i])

        for i in range(len(temp2)):
            temp2[i]=float(temp2[i])

        self.axes.plot(hora, temp1, 'o-', linewidth=6, label='Sensor 1',  color ='tab:blue')
        self.axes.plot(hora, temp2, 'x-', linewidth=1, label='Sensor 2', color ='tab:red')
        self.axes.set_title('Gráfico da variação da temperatura dos sensores 1 e 2 ao longo do tempo', fontsize='19')
        self.axes.set_xlabel('Horário (hh:mm:ss)', fontsize='12')
        self.axes.set_ylabel('Temperatura (°C)', fontsize='12')
        self.axes.legend(['Sensor 1', 'Sensor 2'],
          title="Legenda",
          loc="best")
        
        
        lista[0]=hora[0]
        if(len(hora)==2):
            lista[-1]=hora[-1]
        elif(len(hora)>2):
            lista[-2]=''
            
            lista[int(((len(lista)/2)))-1]=''
            lista[int(((len(lista)/2)))]=hora[int(((len(lista)/2)))]
            
            lista[int((len(lista)/4))-1]=''
            lista[int((len(lista)/4))]=hora[int((len(lista)/4))]
        
            lista[int((3*(len(lista)/4)))-1]=''
            lista[int((3*(len(lista)/4)))]=hora[int((3*(len(lista)/4)))]
                
            lista[-1]=hora[-1]
        #print(hora, '\n', lista)
        
        
        self.axes.set_xticks(lista)
        self.axes.set_xticklabels(lista, rotation=0)
        
        self.axes.yaxis.grid()

        
        # print('esse:', temp1, temp2, sorted(set(temp1+temp2)))

        lista.append('')

        
        self.draw()


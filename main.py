import sys

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QMainWindow


from gui import Ui_MainWindow
import dadosSensores as dS

temp1 = []
temp2 = []
hora = []
cont=1

class ControlMainWindow(QMainWindow):
    def __init__(self,parent=None):
        QMainWindow.__init__(self,parent)
        self.gui = Ui_MainWindow()
        self.gui.setupUi(self)
        self.updateData()
        self.timer = QtCore.QTimer(self)
        self.sensors = QtCore.QTimer(self)
        self.timer.timeout.connect(self.updateTime)
        self.timer.timeout.connect(self.updateData)
        self.sensors.timeout.connect(self.plotGraph)
        self.timer.setInterval(2000)
        self.timer.start(1000)
        self.sensors.start(1000)
        


    def updateData(self):
        global temp1
        global temp2
        global hora
        global cont
        temp = dS.getTemp()
      
        
        tm1 = round(float(temp[1]), 2)
        tm2 = round(float(temp[3]), 2)
        media = (tm1+tm2)/2.0
        
        hora.append(QtCore.QTime.currentTime().toString())
        
        temp1.append(temp[1]) #Ajusta a temperatura e adiciona na lista
        temp2.append(temp[3])
        
        if (cont==43200): #A cada 12h
            hora = hora[-3600:]
            temp1 = temp1[-3600:]
            temp2 = temp2[-3600:]
            cont=1
            
        cont+=1

        if media >= 17.0:
            self.gui.alertaTemp()
            #EnviaMen()
        self.gui.lcdNumber.display(temp[1])
        self.gui.lcdNumber_2.display(temp[3])
        
        #print(temp1, temp2)
    
    def plotGraph(self):
        
        self.gui.widget_2.update_figure(hora, temp1, temp2)
        #self.gui.widget_2.update_figure(hora, temp2)

    def updateTime(self):
        global hora
        current = QtCore.QDateTime.currentDateTime()
        self.gui.dateTimeEdit.setDateTime(current.currentDateTime())

if __name__=="__main__":
    #time.sleep(1)
    app = QtWidgets.QApplication(sys.argv)
    win = ControlMainWindow()
    win.show()
    sys.exit(app.exec_())

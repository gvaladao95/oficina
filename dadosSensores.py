
import time
import re
from datetime import datetime
import socket
from cryptography.fernet import Fernet


HOST = '192.168.2.132'     # Endereco IP do Servidor
PORT = 5001         # Porta que o Servidor esta
server_address = (HOST,PORT)



def SalvaLog(Temperatura):
    ArquivoLog = open('arqLog.txt','a')
    ArquivoLog.write("S1: " + str(Temperatura[1]) + " C. " + "S2: " + str(Temperatura[3]) + " C - " + datetime.now().strftime('%d/%m/%Y %H:%M:%S') + "\n")
    ArquivoLog.close()


## Os comentados estou testando pra parte da tela
# def ExtraiDados(dados):
#     temp = re.findall(r"[-+]?\d*\.\d+|\d+", dados)
#     [float(i) for i in temp]
#     return temp



def ExtraiDados(dados):
    f = Fernet("cGbijRXliAs9Zu6p4vJyA0eOXTXVN6sVSU7NNT6PR0k=")
    temp = f.decrypt(dados)
    temp = temp.decode('utf-8')
    temp = re.findall(r"[-+]?\d*\.\d+|\d+",temp)
    [float(i) for i in temp]
    return temp


def getTemp(): # Talvez mude pra uma função
    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp.connect(server_address)
    
    data = tcp.recv(1024)
    #data = data.decode('utf-8')
 



    try:

        if data:
            msg = ExtraiDados(data)
            
            SalvaLog(msg)
            
            #print(msg)
            return msg
    finally:
        time.sleep(1)
        tcp.close()
